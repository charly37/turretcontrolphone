import time
import tornado.ioloop
import tornado.web
import tornado.websocket

class Handler_Main(tornado.web.RequestHandler):
    """Class handling the Web requests"""

    def get(self):
        """Methode call when the server receive a GET HTTP request"""
        self.render("index.html")

aApplication = tornado.web.Application([(r"/", Handler_Main)])
aApplication.listen(8080)
#Start reactor
tornado.ioloop.IOLoop.instance().start()


